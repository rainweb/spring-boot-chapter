package org.minbox.chapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootBasicAccessingApplicationArgumentsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootBasicAccessingApplicationArgumentsApplication.class, args);
    }

}
